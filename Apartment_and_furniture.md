---
title: "Apartment and furniture"
date: \today
author: Dr. Momcilo Krunic
city: Munich
country: Germany
email: momcilo.krunic@labsoft.dev
numbersections: true
---
# Apartment in Welfenstrasse 14a {#mainmany}

![The chillout area on the roof offers a stunning view that truly captivates the senses, making it the perfect spot to unwind and relax.](resources/apartment/drive-download-20230422T115839Z-001/top1.jpg){#fig:top1 width=100%}

Welcome to a sneak peek of our beautiful apartment! We have included pictures of all the rooms in the apartment, in the following order: 

* [the cozy living room](#living)

* [the luxurious bedroom](#bedroom)

* [the fun-filled children's room](#children)

* [the spacious hallway](#hallway)

* [the sparkling bathrooms](#bathrooms)

* [the stunning view from the apartment](#apartment)

Enjoy the tour!

---

# The cozy living room {#living}
![](resources/apartment/drive-download-20230422T115839Z-001/living1.jpg){#fig:living1 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/living2.jpg){#fig:living2 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/living3.jpg){#fig:living3 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/dining1.jpg){#fig:living4 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/dining2.jpg){#fig:living5 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/balcon1.jpg){#fig:living6 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/balcon2.jpg){#fig:living7 width=100%}

---

# The luxurious bedroom {#bedroom}
[Back to the top.](#mainmany)

![](resources/apartment/drive-download-20230422T115839Z-001/bad2.jpg){#fig:bad2 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bad1.jpg){#fig:bad1 width=100%}

---

# The fun-filled children's room {#children}
[Back to the top.](#mainmany)

![](resources/apartment/drive-download-20230422T115839Z-001/kids1.jpg){#fig:kids1 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/kids2.jpg){#fig:kids2 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/kids3.jpg){#fig:kids3 width=100%}

---

# The spacious hallway {#hallway}
[Back to the top.](#mainmany)



![](resources/apartment/drive-download-20230422T115839Z-001/hall2.jpg){#fig:hall2 width=100%}

---

# The sparkling bathrooms {#bathrooms} 
[Back to the top.](#mainmany)

![](resources/apartment/drive-download-20230422T115839Z-001/bath1.jpg){#fig:bath1 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bath2.jpg){#fig:bath2 width=100%}



![](resources/apartment/drive-download-20230422T115839Z-001/bath4.jpg){#fig:bath4 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bath5.jpg){#fig:bath5 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bath6.jpg){#fig:bath6 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bath7.jpg){#fig:bath7 width=100%}



---

# The stunning view from the apartment {#apartment}
[Back to the top.](#mainmany)

![](resources/apartment/drive-download-20230422T115839Z-001/outside1.jpg){#fig:outside1 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/outside2.jpg){#fig:outside2 width=100%}

---


# Furniture for sale {#sale}

[Back to the top.](#mainmany)



If you're in love with the stunning furniture in the photos, good news: all of it can be yours for the right price. And the best part? We're open to negotiation. As a new tenant, you should take over the three most important pieces - the kitchen, the hallway wardrobe, and the bedroom wardrobe - for the unbeatable price of **10500 EURO**. Don't miss out on the opportunity to make this your dream apartment!

* [Kitchen](#kitchen) - Separate price tag 8900 EURO (Paid 13600 EURO a year and a half ago)
* [Wardrobe in the hallway](#hallway1) - Separate price tag 950 EURO (Paid 1850 EURO a year and a half ago)
* [Wardrobe in the bedroom](#bedroom1) - Separate price tag 1050 EURO (Paid 1950 EURO a year and a half ago)

The kitchen and wardrobes are in excellent condition and were custom-made for this apartment just 1.5 years ago. They fit perfectly and were crafted using high-quality materials and supplies to enhance the overall look and feel of the space.

## Kitchen {#kitchen}
[Back to the main many.](#sale)


![Kitchen front view.](resources/apartment/drive-download-20230422T115839Z-001/kitchen1.jpg){#fig:kitchen1 width=100%}

![Kitchen front view.](resources/apartment/drive-download-20230422T115839Z-001/kitchen2.jpg){#fig:kitchen2 width=100%}

![Kitchen front view.](resources/apartment/drive-download-20230422T115839Z-001/kitchen3.jpg){#fig:kitchen3 width=100%}

![Kitchen front view.](resources/apartment/drive-download-20230422T115839Z-001/kitchen4.jpg){#fig:kitchen4 width=100%}



![Kitchen front view.](resources/apartment/drive-download-20230422T115839Z-001/kitchen6.jpg){#fig:kitchen6 width=100%}

![Dishwasher.](resources/apartment/drive-download-20230422T115839Z-001/kitchen7.jpg){#fig:kitchen7 width=100%}



![Dishwasher.](resources/apartment/drive-download-20230422T115839Z-001/kitchen9.jpg){#fig:kitchen9 width=100%}



![Oven.](resources/apartment/drive-download-20230422T115839Z-001/kitchen11.jpg){#fig:kitchen11 width=100%}

![Hot plate.](resources/apartment/drive-download-20230422T115839Z-001/kitchen12.jpg){#fig:kitchen12 width=100%}



---



## Wardrobe in the hallway  {#hallway1}
[Back to the main many.](#sale)

![](resources/apartment/drive-download-20230422T115839Z-001/hall2.jpg){#fig:hall2 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall3.jpg){#fig:hall3 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall4.jpg){#fig:hall4 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall5.jpg){#fig:hall5 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall6.jpg){#fig:hall6 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall7.jpg){#fig:hall7 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/hall8.jpg){#fig:hall8 width=100%}

---



## Wardrobe in the bedroom {#bedroom1}
[Back to the main many.](#sale)

![](resources/apartment/drive-download-20230422T115839Z-001/bad3.jpg){#fig:bad3 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bad4.jpg){#fig:bad4 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bad5.jpg){#fig:bad5 width=100%}

![](resources/apartment/drive-download-20230422T115839Z-001/bad6.jpg){#fig:bad6 width=100%}