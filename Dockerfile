FROM debian:latest

RUN apt-get update && apt-get install -y \
    xz-utils \
    make \
    wget \
    librsvg2-bin

RUN wget -c https://github.com/jgm/pandoc/releases/download/2.13/pandoc-2.13-1-amd64.deb 
RUN wget -c https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.10.0a/pandoc-crossref-Linux.tar.xz 

RUN apt-get install -y \ 
    ./pandoc-2.13-1-amd64.deb \
    tar \
    texlive \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN tar -xf pandoc-crossref-Linux.tar.xz 
RUN mv pandoc-crossref /usr/local/bin/ 
RUN chmod a+x /usr/local/bin/pandoc-crossref






