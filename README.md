# Apartment and Furniture in Welfenstrasse 14a

PDF: https://momcilo_krunic.gitlab.io/apartment-in-munich-and-furniture/Apartment_and_furniture.pdf

DOCX: https://momcilo_krunic.gitlab.io/apartment-in-munich-and-furniture/Apartment_and_furniture.docx

ODT: https://momcilo_krunic.gitlab.io/apartment-in-munich-and-furniture/Apartment_and_furniture.odt

Translated by google:

DOCX: [resources/translated/Wohnung_und_Möbel.docx](resources/translated/Wohnung_und_Möbel.docx)

PDF: [resources/translated/Wohnung_und_Möbel.pdf](resources/translated/Wohnung_und_Möbel.pdf)
